import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.CardLayout;
import java.awt.GridLayout;
import javax.swing.Timer;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


class Pinball extends JFrame {
    MyPanel mypanel;
    JPanel cardpanel;

    CardLayout layout;

    Pinball() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(650, 400);
        setTitle("Pinball");
        layout = new CardLayout();

        cardpanel = new JPanel();
        cardpanel.setLayout(layout);

        mypanel = new MyPanel();
        cardpanel.add(mypanel, "mypanel");

        add(cardpanel);
    }
    public static void main(String[] args) {
        Pinball f = new Pinball();
        Config config = new Config(f);
    }
}
class Config extends JFrame implements ActionListener {
    JButton button1;
    JButton button2;
    JButton button3;
    JButton button4;

    Pinball pinball;
    boolean isexist;

    Config(Pinball panel) {
        pinball = panel;
        isexist=false;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(50,50,100,300);
        setTitle("Config");

        button1 = new JButton("Fixed_battery");
        button2 = new JButton("Remove_ball");
        button3 = new JButton("Reset");
        button1.addActionListener(this);
        button2.addActionListener(this);
        button3.addActionListener(this);
        button1.setActionCommand("Fixed_battery");
        button2.setActionCommand("Remove_ball");
        button3.setActionCommand("Reset");

        JPanel p = new JPanel();
        p.setLayout(new GridLayout(4, 1));
        p.add(button1);
        p.add(button2);
        p.add(button3);
        this.add(p);

        setVisible(true);
    }
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();

        if (cmd.equals("Fixed_battery")) {
            pinball.mypanel.restart(new Fixed_battery(pinball.mypanel));
        }
        else if (cmd.equals("Remove_ball")) {
            pinball.mypanel.restart(new Removeball(pinball.mypanel));
        }
        else if (cmd.equals("Reset")) {
            pinball.mypanel.reset();
        }
        else {
            System.out.println("pressed unknown button");
            return;
        }
        if (!pinball.mypanel.isstarted){
            pinball.mypanel.start();
            pinball.mypanel.isstarted = true;
        }
        if (!isexist){
            pinball.setBounds(getX()+getWidth(),getY(),650,400);
            pinball.setVisible(true);
            isexist=true;
        }
    }
}
class MyPanel extends JPanel {
    final Balls balls;
    Timer timer;
    BallAction ballaction;
    boolean isstarted=false;

    public MyPanel() {
        balls = new Balls(this);
        balls.push(new MyBall(this));
    }
    public void setBallAction(BallAction ballaction) {
        this.ballaction = ballaction;
    }
    public void start() {
        // ballの座標を画面左下隅に配置
        balls.top.goHome();
        timer.start();
        isstarted=true;
    }
    public void reset() {
        while (balls.top!=null) 
            balls.pop();
    }
    public void restart(BallAction ballaction) {
        removeMouseListener(this.ballaction);
        removeMouseMotionListener(this.ballaction);
        this.ballaction=ballaction;
        addMouseListener(ballaction);
        addMouseMotionListener(ballaction);
        timer = new Timer(50, ballaction);
    }
    public void paintComponent(Graphics g) {
    	super.paintComponent(g);
    	balls.draw(g);
    	ballaction.drawmouseline(g);
    }
}
