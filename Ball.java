import java.awt.Graphics;
import java.awt.Color;

class Ball {
    public MyPanel myPanel;
    public double xx, yy;    // ボールの中心座標
    public double dx, dy; // 速度=X方向とY方向の移動距離
    public double rr;           // 半径
    public Color color;
    public double GG; // 重力加速度
    public double KK; // 反発係数
    
    Ball(MyPanel mp) {
        myPanel = mp;
        init();
    }
    Ball(MyPanel mp, double x, double y, double dx, double dy, 
         double rr, Color col) {
        myPanel = mp;
        init();
        xx = x; yy = y;
        this.dx = dx; this.dy = dy;
        this.rr = rr; color = col;
    }
    void init() {
    	xx = 0.0; yy = 0.0;
    	//dx = 15; dy = -15;
    	//rr = 10;
    	dx = 5; dy = -5;
    	rr = 30;
    	color = Color.black;
    	GG = 0.5;
    	KK = 1.0;
    }
    double getX() { return xx; }
    double getY() { return yy; }
    double getDX() { return dx; }
    double getDY() { return dy; }
    double getR() { return rr; }
    void setX(double x) { xx = x; }
    void setY(double y) { yy = y; }
    void setXY(double x, double y) { xx = x; yy = y; }
    void setG(double g) { GG = g; }
    void setK(double k) { KK = k; }

    // 画面左下隅へ移動
    void goHome() {
        xx = rr; 
        yy = myPanel.getHeight() - rr; 
    }
    void setDX(double dx) { this.dx = dx; }
    void setDY(double dy) { this.dy = dy; }
    void setDXY(double dx, double dy) { this.dx = dx; this.dy = dy; }
    void setR(double r) { rr = r; }
    void setColor(Color col) {
        color = col;
    }
    // 描画
    void draw(Graphics g) {
        Color backup = g.getColor();
        g.setColor(color);
        int d = (int)(rr + rr);
        g.fillOval((int)(xx - rr), (int)(yy - rr), d, d);
        g.setColor(backup);
    }

    // 座標更新
    void next() {
        // 画面の幅と高さを取得 (画面のサイズ変更に備える）
        double width = myPanel.getWidth();   
        double height = myPanel.getHeight();

        // ball の座標更新
        xx += dx;
        yy += dy;
        if (xx - rr < 0) { // 左の壁にぶつかった
            xx = rr;
            dx = -dx * KK;
        } 
        else if (xx + rr > width) { // 右の壁にぶつかった
            xx = width - rr;
            dx = -dx * KK;
        }
        if (yy - rr < 0) { // 天井にぶつかった
            yy = rr;
            dy = -dy * KK;
        }
        else if (yy + rr > height) { // 床にぶつかった
            yy = height - rr;
            dy = -dy * KK;
        }
        dy += GG; // 重力加速度を考慮
    }
}
