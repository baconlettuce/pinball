import java.awt.Point;
import java.awt.event.*;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.*;

class BallAction
	implements ActionListener, MouseListener, MouseMotionListener {
	MyPanel mypanel;

	BallAction(MyPanel mypanel) {
		this.mypanel = mypanel;
	}
	public void actionPerformed(ActionEvent e) {
        mypanel.balls.next();
        mypanel.repaint();
    }
    public void drawmouseline(Graphics g) {}
    public void mousePressed(MouseEvent e) {}
    public void mouseReleased(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
    public void mouseMoved(MouseEvent e) {}
    public void mouseDragged(MouseEvent e) {}
    public void mouseClicked(MouseEvent e) {}
}
class Fixed_battery extends BallAction {
    Point pmousepoint;
    Point cmousepoint;
    boolean ismousepressed=false;

	Fixed_battery(MyPanel mypanel) {
		super(mypanel);
		ismousepressed=false;
	}
	public void drawmouseline(Graphics g) {
		if (pmousepoint==null||cmousepoint==null || !ismousepressed)
			return;

		Graphics2D g2 = (Graphics2D)g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, 
			RenderingHints.VALUE_ANTIALIAS_ON);
		if (pmousepoint==null|| cmousepoint==null)
			return;
		g2.draw(new Line2D.Double(pmousepoint, cmousepoint));
	}
	public void mousePressed(MouseEvent e) {
        pmousepoint = e.getPoint();
        ismousepressed = true;
    }
    public void mouseReleased(MouseEvent e) {
    	cmousepoint = e.getPoint();
        MyBall ball = new MyBall(mypanel);
        ball.goHome();
        ball.setDX(Math.abs(pmousepoint.x-cmousepoint.x));
        ball.setDY(Math.abs(pmousepoint.y-cmousepoint.y));
        mypanel.balls.push(ball);

        ismousepressed = false;
        pmousepoint    = null;
        cmousepoint    = null;
    }
    public void mouseDragged(MouseEvent e) {
        cmousepoint = e.getPoint();
    }
}
class Removeball extends BallAction {
	Removeball(MyPanel mypanel) {
		super(mypanel);
	}
    public void mousePressed(MouseEvent e) {
        mypanel.balls.remove(e.getPoint());
    }
}