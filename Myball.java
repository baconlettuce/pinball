import java.awt.Graphics;
import java.awt.Color;
import java.awt.Point;

class MyBall extends Ball {
	MyBall next;
	
	MyBall(MyPanel mp){
		super(mp);
	}
    MyBall(MyPanel mp, double x, double y, double dx, double dy, 
         double rr, Color col) {
    	super(mp,x,y,dx,dy,rr,col);
    }
    void setnext(MyBall ball) {
    	next = ball;
    }
}

class Balls {
	MyPanel myPanel;
	MyBall top;
	MyBall bottom;

	Balls (MyPanel panel) {
		myPanel = panel;
	}
	public void push (MyBall ball) {
		if (top==null) {
			top = ball;
			bottom = ball;
			return;
		}

		ball.next = top;
		top = ball;
	}
	public MyBall pop () {
		if(top == null)
			return null;

		MyBall ball = top;
		if(top == bottom) {
			top    = null;
			bottom = null;
			return ball;
		}
		top = top.next;
		return ball;
	}
	public int size() {
		int count = 0;
		for (MyBall ball=top; ball!=null; ball=ball.next)
			count++;
		return count;
	}
	public void draw(Graphics g) {
		for(MyBall ball=top; ball!=null; ball=ball.next) {
			ball.draw(g);
		}
	}
	public void next() {
		for(MyBall ball=top; ball!=null; ball=ball.next) {
			ball.next();
		}
	}
	/*
	public Myball search(Graphics g) {
		for(MyBall balla=top; balla!=null; balla=balla.next) {
			double ax=balla.getX();
			double ay=balla.getY();
			for(Myball ballb=top; ballb!=null; ballb=ballb.next) {

			}
		}
	}
	*/
	public void remove(Point point) {
		MyBall last=null;
		for (MyBall ball=top; ball!=null; ball=ball.next) {
			double xx = ball.getX();
			double yy = ball.getY();
			double rr = ball.getR();
			if ( !( Math.pow(((double)point.x-xx), 2) + Math.pow((double)point.y-yy, 2) - Math.pow(rr, 2) <= 0)) {
				last = ball;
				continue;
			}
			else if (ball==top && ball.next==null) {
				top=null;
				return;
			}
			else if (ball==top) {
				top = top.next;
				return;
			}
			else if (ball.next==null) {
				last.next=null;
				return;
			}
			else {
				last.next=ball.next;
				return;
			}
		}
	}

}
